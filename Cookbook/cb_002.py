# %%
"""
CressetFlareCB_2 Remove chains of type 'other'

Scope: Delete all chains of type ‘Other’ from all proteins
Platform: This snippet works on proteins already loaded into a Flare
    project: either into a project open in the GUI or into a project
    (.flr) saved in the specified working directory.
"""

# %%
# Setup: Loading libraries, set working directory, open a project if needed
import os
from cresset import flare

work_dir = os.path.expanduser('~/Documents/Flare/Cookbook')
os.chdir(work_dir)

flare_project = None

main_window = flare.main_window()

if main_window:
    project = flare.main_window().project
else:
    flare_project = 'myflareproject.flr'
    project = flare.Project.load(flare_project)

# Remove "Other" sequences
count = 0
for protein in project.proteins:
    for seq in protein.sequences:
        if seq.type == flare.Sequence.Type.Other:
            print(protein.title, 'Removing:', seq)
            protein.sequences.remove(seq)
            count += 1
print('Removed', count, 'sequence(s)')

# Save project
if flare_project:
    project.save(flare_project)
