# %%
"""
CressetFlareCB_13 Prepare a large number proteins without pre-loading them in the GUI

Scope: Go through proteins in WD/INPUT and prepare them using the
    default protein preparation method in Flare. Useful when
    processing a large number of proteins which cannot be easily
    handled in the Flare GUI.
Platform: Protein files contained in WD/INPUT. This snippet requires
    an open Flare project to run successfully, but does not require
    the protein files to be pre-loaded into the project.
Notes: The unprepared protein .pdb files should be in the Flare
    WD/INPUT, with the suffix '_protein.pdb'. The prepared proteins
    will be saved in Flare WD/OUTPUT, but may need to be moved into
    WD/INPUT to use the prepared proteins as an input for other
    snippets. Please refer to CressetFlareCB_17 for information on how
    to move protein files into the WD.
"""

# %%
# Setup: Loading libraries, set working directory, open a blank project
import os, glob
from cresset import flare

work_dir = os.path.expanduser('~/Documents/Flare/Cookbook')
os.chdir(work_dir)

flare_project = None

out_dir = os.path.join(work_dir, 'OUTPUT')
os.makedirs(out_dir, exist_ok=True)

main_window = flare.main_window()

if main_window:
    project = flare.main_window().project
else:
    flare_project = 'myflareproject'
    project = flare.Project()

# Locate your data folder
data_dir = os.path.expanduser('~/Documents/EXAMPLEDATA')

### Core snippet ###
for protein_file in glob.glob(f'{data_dir}/**/*_protein.pdb'):
    project.proteins.extend(flare.read_file(protein_file, 'pdb'))
    print('Read:', project.proteins[-1].title, 'from', protein_file)

# Prep only unpreppred proteins
prep = flare.ProteinPrep()
proteins_to_prep = []
for protein in project.proteins:
    if not protein.is_prepared:
        print('Will prep:', protein.title)
        proteins_to_prep.append(protein)

# Prep processing
prep.proteins = proteins_to_prep
prep.start()
prep.wait()

# Write proteins to separate PDBs
for protein in proteins_to_prep:
    output = os.path.join(out_dir, f'{protein.title}_protein_P.pdb')
    flare.write_file(output, [protein], 'pdb')
    print(f'Wrote prepped {protein.title} to {output}')

# Save project
if flare_project:
    project.save(flare_project)
