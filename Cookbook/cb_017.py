# %%
"""
CressetFlareCB_17 Detect and move input data files

Scope: Data that has been downloaded from external sources such as the
    PDB will need to be moved into the WD/INPUT directory for use with
    the Flare Python Cookbook. The present snippet will walk down from
    working directory and move any ligand or protein structure files
    to the WD/INPUT directory. Ligands are identified as files ending
    with '_ligand.sdf' and proteins are identified as files ending
    with '_proteins.pdb'.
Notes: Downloaded data files, e.g. from the PDB are usually in a main
    folder, followed by subfolders. This main folder should be placed
    inside the WD. The specific name of the main data folder does not
    matter here.
"""

# %%
# Setup: Loading libraries, set working directory
import os, shutil

work_dir = os.path.expanduser('~/Documents/Flare/Cookbook')
os.chdir(work_dir)

in_dir = 'INPUT'
os.makedirs(in_dir, exist_ok=True)

# Locate your data folder
data_dir = os.path.expanduser('~/Documents/EXAMPLEDATA')

# Detect and copy
for root, dirs, files in os.walk(data_dir):
    for name in files:
        if name.endswith('_protein.pdb') or name.endswith('_ligand.sdf'):
            full_path = os.path.join(root, name)
            print(f'Copy: {full_path} to {in_dir}')
            shutil.copy(full_path, in_dir)
