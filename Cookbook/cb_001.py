# %%
"""
CressetFlareCB_1 Remove non-A-chains

Scope: Delete all chains from all proteins in Flare, except for the
    'A' chains. Any A-Water and A-Other chains are unaffected by this
    snippet.
Platform: This snippet works on proteins already loaded into a Flare
    project: either in a project open in the GUI or in a project
    (.flr) saved in the specified the working directory.
"""

# %%
# Setup: Loading libraries, set working directory, open a project if needed
import os, sys
from cresset import flare

work_dir = os.path.expanduser('~/Documents/Flare/Cookbook')
os.chdir(work_dir)

flare_project = None

main_window = flare.main_window()

if main_window:
    project = flare.main_window().project
else:
    flare_project = 'myflareproject.flr'
    project = flare.Project.load(flare_project)

# Remove non-A chains
count = 0
for protein in project.proteins:
    for seq in protein.sequences:
        if seq.chain != 'A':
            print(protein.title, 'Removing:', seq)
            protein.sequences.remove(seq)
            count += 1
print('Removed', count, 'sequence(s)')

# Save project
if flare_project:
    project.save(flare_project)
