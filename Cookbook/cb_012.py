# %%
"""
CressetFlareCB_12 Calculate log_Kd from LF_dG

Scope: Calculate log Kd from LF dG and add column to Flare Ligands
    Table - requires ligand to be docked using LeadFinder.
Platform: This snippet works on proteins already loaded into a Flare
    project: either into a project open in the GUI or into a project
    (.flr) saved in the specified working directory.
"""

# %%
# Setup: Loading libraries, set working directory, open a project if needed
import os, math
from cresset import flare

wd = os.path.expanduser('~/Documents/Flare/Cookbook')
os.chdir(wd)

flare_project = None

main_window = flare.main_window()

if main_window:
    project = flare.main_window().project
else:
    flare_project = 'myflareproject.flr'
    project = flare.Project.load(flare_project)

# Constants
R = 1.9858775e-3
T = 298.15
k = math.log10(math.e)

# Put logKd in ligand property 
for ligand in project.ligands:
    dG = ligand.properties['LF dG'].value
    if dG is None:
        continue
    logKd = -dG/(R*T)*k
    ligand.properties['Log_Kd'].value = round(logKd, 3)

# Save project
if flare_project:
    project.save(flare_project)
