# %%
"""
CressetFlareCB_18 Reading in example ligands and proteins

Scope: This snippet simply reads in example ligand/protein files to
    help users test the snippets in this cookbook on their own
    computer system. **An 'EXAMPLEDATA' zipped folder containing the
    example files is available to download.**
Notes: The directory containing the example files should be a
    subdirectory of the Cookbook WD. Please leave this subdirectory
    named 'EXAMPLEDATA', so that it will be ignored by
    CressetFlareCB_17.
"""

# %%
# Setup: Loading libraries, set working directory, open a blank project
import os, glob
from cresset import flare

work_dir = os.path.expanduser('~/Documents/Flare/Cookbook')
os.chdir(work_dir)

in_dir = os.path.join(work_dir, 'INPUT')

main_window = flare.main_window()

if main_window:
    project = flare.main_window().project
else:
    project = flare.Project()

# Read proteins from PDB files
for protein_file in glob.glob(f'{in_dir}/*_protein.pdb'):
    print(protein_file)
    project.proteins.extend(flare.read_file(protein_file, 'pdb'))

# Read ligands from SDF files
for ligand_file in glob.glob(f'{in_dir}/*_ligand.sdf'):
    print(ligand_file)
    project.ligands.extend(flare.read_file(ligand_file, 'sdf'))

# Associate proteins
for ligand in project.ligands:
    for protein in project.proteins:
        if protein.title.lower() == ligand.title[:4].lower():
            ligand.protein = protein
            print('Associated:', ligand.title, 'to', protein.title)
            break

# Save project
flare_project = 'myflareproject.flr'
project.save(flare_project)
print('Wrote', len(project.ligands), 'ligands and', len(project.proteins), 'proteins to', flare_project)
