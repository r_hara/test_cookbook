# %%
"""
## Introduction

This document provides Flare users with Python code examples to use with the Flare Python API and pyflare. The Flare Python API allows Flare functionality to be accessed from Python, and also enables you to add functionality to Flare’s graphical user interface (GUI). Each 'snippet' or 'recipe' in the Cookbook is intended to work with little or no user editing, but is equally intended to serve as a starting point for users who wish to develop their own snippets. Individual snippets can simply be pasted into the Flare Python Interpreter window within the Flare GUI, and run. Some snippets should be run in a particular order, e.g. where data is expected to be read in a particular format. Whenever a snippet has a prerequisite snippet, this is made clear in the notes at the beginning of the recipe. Contact us at support@cresset-group.com if you need a new snippet or recipe.
"""

# %%
"""
### Creating the working environment

To familiarize with the snippets and recipes in this Cookbook, we recommend new users to run them from within an open Flare GUI.

However, confident users may wish to run the snippets and recipes on an unopen myflareproject.flr file, which should be located within the Cookbook Working Directory (WD - see below). In this case, snippets may be from any application supporting Python execution where the pyflare.exe kernel selected.

1. **Create a directory named 'Cookbook'**, inside the default Documents/Flare directory for your OS:

Windows: C:/Users/<USER>/Documents/Flare

Linux: ~/Documents/Flare

macOS: ~/Documents/Flare

E.g. C:\Users\Foobar\Documents\Flare\Cookbook on Windows. 

**This is the Cookbook WD.**

![static/directory_structure.png](~/Documents/Flare/Cookbook/static/directory_structure.png)

2. Inside the WD, create two **sub**-directories named 'INPUT' and 'OUTPUT'. These will be used by all snippets and recipes, therefore it is important to create these directories prior to using the Flare Python Cookbook.

Confident users may want to create their own '### Setup ###' section, where the standard working directory paths may be changed, though we do not recommend this for users who are new to the Flare Python API.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
"""
