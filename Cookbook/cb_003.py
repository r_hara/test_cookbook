# %%
"""
CressetFlareCB_3 Remove unprepared proteins

Scope: Delete all protein structures whose title in the Flare Proteins
    table does not include a '_P' flag. Typically, these are
    unprepared protein structures.
Platform: This snippet works on proteins already loaded into a Flare
    project: either into a project open in the GUI or a into project
    (.flr) saved in the specified working directory.
"""

# %%
# Setup: Loading libraries, set working directory, open a project if needed
import os
from cresset import flare

work_dir = os.path.expanduser('~/Documents/Flare/Cookbook')
os.chdir(work_dir)

flare_project = None

main_window = flare.main_window()

if main_window:
    project = flare.main_window().project
else:
    flare_project = 'myflareproject.flr'
    project = flare.Project.load(flare_project)

# Remove unprepared proteins
count = 0
for protein in project.proteins:
    if not protein.is_prepared:
        print('Removing:', protein.title)
        project.proteins.remove(protein)
        count += 1
print('Removed', count, 'protein(s)')

# Save project
if flare_project:
    project.save(flare_project)
