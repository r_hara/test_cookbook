import argparse, sys, os, subprocess as sp

def put_props(props, file):
    author = props.get('Author', 'Cresset')
    if 'Author' in props:
        del props['Author']
    title = props.get('Title')
    del props['Title']
    index = props.get('Index')
    del props['Index']
    print(f'## {title}', file=file)
    print(file=file)
    for key in props:
        print(f'**{key}**', props[key], file=file)
        print(file=file)
    print(f'**Index**:', index, file=file)
    print(file=file)
    print(f'**Author**:', author, file=file)
    print(file=file)

def parse_inc(name, file=sys.stdout):
    print('include:', name, file=sys.stderr)
    lines = open(inc_name, 'rt').read().strip().split('\n')
    i = 0
    status = None
    props = {}
    cur_prop = None
    while i < len(lines):
        if status == 'descriptions' and lines[i] == '"""':
            put_props(props, file=file)
            print(lines[i], file=file)
            print(file=file)
            props.clear()
            status = None
            i += 1
            continue

        if status == 'markdown' and lines[i] == '"""':
            print(lines[i], file=file)
            print(file=file)
            status = None
            i += 1
            continue

        if status == 'descriptions' and lines[i] == '':
            i += 1
            continue

        if status == 'descriptions' and not lines[i].startswith(' '):
            print(lines[i], file=file)
            it = lines[i].split(':', maxsplit=1)
            cur_prop = it[0]
            value = it[1].strip()
            props[cur_prop] = value
            i += 1
            continue

        if status == 'descriptions' and lines[i].startswith(' '):
            props[cur_prop] += ' ' + lines[i].strip()
            i += 1
            continue

        if lines[i] == '# %%' and lines[i+1] == '"""' and lines[i+2].startswith('#'):
            print(lines[i], file=file)
            print(lines[i+1], file=file)
            print(lines[i+2], file=file)
            status = 'markdown'
            i += 3
            continue

        if lines[i] == '# %%' and lines[i+1] == '"""':
            print(lines[i], file=file)
            print(lines[i+1], file=file)
            it = lines[i+2].split(' ', maxsplit=1)
            props['Index'] = it[0]
            props['Title'] = it[1]
            status = 'descriptions'
            i += 3
            continue

        print(lines[i], file=file)
        if i == len(lines) - 1:
            print(file=file)
        i += 1

parser = argparse.ArgumentParser()
parser.add_argument('iname')
args = parser.parse_args()

iname = args.iname
dirname = os.path.dirname(iname)
print(f'input: {iname}', file=sys.stderr)

oname = os.path.basename(iname)[:-3] + '.py'
out = open(oname, 'wt')

for line in open(iname, 'rt'):
    line = line.strip()
    if line.startswith('# %Include '):
        inc_name = os.path.join(dirname, line[11:])
        print('include:', inc_name, file=sys.stderr)
        parse_inc(inc_name, file=out)
    else:
        print(line, file=out)

print(f'output: {oname}', file=sys.stderr)
out.close()

# strip
cont = open(oname, 'rt').read().strip()
open(oname, 'wt').write(cont + "\n")

notebook = oname[:-3] + '.ipynb'
command = [sys.executable, '-m', 'ipynb_py_convert', oname, notebook]
sp.run(command)
